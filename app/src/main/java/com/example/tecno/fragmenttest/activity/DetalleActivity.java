package com.example.tecno.fragmenttest.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.tecno.fragmenttest.R;
import com.example.tecno.fragmenttest.fragments.DetalleFragment;
import com.example.tecno.fragmenttest.model.Producto;

/**
 * Created by cgagliardi on 19/10/2016.
 */

public class DetalleActivity extends AppCompatActivity {

    private Producto producto;

    public static final String PROD = "producto";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        final DetalleFragment detalleFragment = DetalleFragment.newInstance(getIntent().getExtras());

        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.add(R.id.detalle_container, detalleFragment, "detalleActivityFragment");
        ft.commit();
    }
}
