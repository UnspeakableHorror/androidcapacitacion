package com.example.tecno.fragmenttest.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by cgagliardi on 26/10/2016.
 */

public class SettingsManager {

    private static SettingsManager INSTANCE;

    static String SETTINGS_PREF = "SETTINGS_PREF";
    static String ONLY_WIFI = "ONLY_WIFI";
    static String SYNCHRONIZATION_TIME = "SYNCHRONIZATION_TIME";
    static String FCM_REGISTRATION = "FCM_REGISTRATION";

    private SharedPreferences sharedPreferences;

    public static SettingsManager getInstance(Context context) {

        if(INSTANCE == null)
            INSTANCE = new SettingsManager(context);
        return INSTANCE;
    }

    private SettingsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(SETTINGS_PREF, Context.MODE_PRIVATE);
    }


    public boolean isOnlyWifi(){
        return sharedPreferences.getBoolean(ONLY_WIFI, false);
    }

    public void setOnlyWifi(boolean onlyWifi){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(ONLY_WIFI, onlyWifi);
        editor.apply();
    }

    public  int getSynchronizationTime() {
        return sharedPreferences.getInt(SYNCHRONIZATION_TIME, -1);
    }

    public void setSynchronizationTime(int time) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SYNCHRONIZATION_TIME, time);
        editor.apply();
    }

    public void setFcmRegistration(boolean register) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(FCM_REGISTRATION, register);
        editor.apply();
    }

    public boolean getFcmRegistraton(){
        return sharedPreferences.getBoolean(FCM_REGISTRATION, false);
    }
}
