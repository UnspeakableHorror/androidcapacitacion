package com.example.tecno.fragmenttest.events;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.tecno.fragmenttest.model.SettingsManager;
import com.example.tecno.fragmenttest.service.DownloadService;

import static android.net.ConnectivityManager.TYPE_MOBILE;
import static android.net.ConnectivityManager.TYPE_WIFI;

/**
 * Created by cgagliardi on 26/10/2016.
 */

public class NetworkReceiver extends BroadcastReceiver {

    static String TAG = NetworkReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean onlyWifi = SettingsManager.getInstance(context).isOnlyWifi();

        //if(!) {
        //    Log.v(TAG, "Sync disabled. Not updating.");
        //    return;
       // }

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        Log.v(TAG, "Network change");

        if(cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isConnectedOrConnecting() ) {
            Log.v(TAG, "Network connected or connecting");

                if(onlyWifi) {
                    if(cm.getActiveNetworkInfo().getType() == TYPE_WIFI) {
                        download(context);
                    } else {
                        Log.v(TAG, "Sync disabled on non wifi. Not updating.");
                    }
                } else {
                    download(context);
                }
        }
    }

    private void download(Context context) {
        Intent download = new Intent(context, DownloadService.class);
        context.startService(download);
    }
}
