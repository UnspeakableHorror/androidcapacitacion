package com.example.tecno.fragmenttest.model;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;

/**
 * Created by cgagliardi on 19/10/2016.
 */

public class GsonRequest<T> extends Request<T> {

    private Gson gson;

    private Class<T> clazz;

    private Response.Listener<T> listener;

    public GsonRequest(int method, String url,  Class<T> clazz, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.listener = listener;
        gson = new Gson();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {


        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

            return Response.success(gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            return Response.error(new VolleyError(e));

        } catch (JsonSyntaxException e) {
            return  Response.error(new VolleyError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }
}
