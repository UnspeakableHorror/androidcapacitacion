package com.example.tecno.fragmenttest.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.tecno.fragmenttest.R;
import com.example.tecno.fragmenttest.fragments.DetalleFragment;
import com.example.tecno.fragmenttest.fragments.ProductosFragment;
import com.example.tecno.fragmenttest.model.Producto;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class HomeActivity extends AppCompatActivity implements ProductosFragment.OnDetailClickListener {

    private boolean esDoblePanel = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setTitle("Home");
        // muestra una flecha
        //getSupportActionBar().setDisplayHomeAsUpEnabled();
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setSubtitle("Productos");

        Intent homeIntent = getIntent();


        if(homeIntent != null && homeIntent.getExtras() != null && homeIntent.getExtras().get(LoginActivity.USER) != null) {
            String user = homeIntent.getExtras().get(LoginActivity.USER).toString();

            Toast.makeText(
                    this,
                    user,
                    Toast.LENGTH_SHORT)
                    .show();
        }

        esDoblePanel = getSupportFragmentManager().findFragmentById(R.id.detalle_fragment) != null;

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
    }

    @Override
    public void onDetailClick(Producto producto) {

        Toast.makeText(
                this,
                "Producto " + producto.getNombre(),
                Toast.LENGTH_SHORT)
                .show();

        if(esDoblePanel){
            DetalleFragment detalle = (DetalleFragment) getSupportFragmentManager().findFragmentById(R.id.detalle_fragment);
            detalle.mostrarDetalle(producto);

        } else {
            Bundle bundle = new Bundle();
            bundle.putSerializable(DetalleActivity.PROD, producto);

            Intent detalle = new Intent(this, DetalleActivity.class);
            detalle.putExtras(bundle);

            this.startActivity(detalle);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.activity_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
