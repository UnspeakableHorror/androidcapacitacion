package com.example.tecno.fragmenttest.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tecno.fragmenttest.R;
import com.example.tecno.fragmenttest.activity.SettingsActivity;
import com.example.tecno.fragmenttest.model.Producto;
import com.example.tecno.fragmenttest.model.ProductoAdapter;
import com.example.tecno.fragmenttest.dao.ProductoDao;
import com.example.tecno.fragmenttest.service.DownloadService;

/**
 * Created by cgagliardi on 18/10/2016.
 */

public class ProductosFragment extends Fragment {

    private ListView productos;

    private OnDetailClickListener callback;

    private DowndloadReceiver downdloadReceiver;

    private IntentFilter intentFilter;

    private Button irAConfig;

    public interface OnDetailClickListener{
        void onDetailClick(Producto producto);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        downdloadReceiver = new DowndloadReceiver();
        intentFilter = new IntentFilter();
        intentFilter.addAction(DownloadService.ACTION_DOWNLOAD_SUCCESS);
        intentFilter.addAction(DownloadService.ACTION_DOWNLOAD_ERROR);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_productos, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        productos = (ListView) view.findViewById(R.id.productos_fragment);

        pedirProductos();

        // productos.setAdapter(new ProductoAdapter(pedirProductos()));

        productos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                callback.onDetailClick((Producto) productos.getItemAtPosition(position));
            }
        });

        //callback.onDetailClick((Producto) productos.getItemAtPosition(0));

        irAConfig = (Button) view.findViewById(R.id.configuracion);

        irAConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent irAConfig = new Intent(ProductosFragment.this.getActivity() , SettingsActivity.class);
                ProductosFragment.this.getActivity().startActivity(irAConfig);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //pedirProductos();
        //callback.onDetailClick((Producto) productos.getItemAtPosition(0));
    }

    private void pedirProductos(){
        String url = "http://www.webkathon.com/pruebasit/products.php";

        /*GsonRequest<Producto[]> request = new GsonRequest<>(
                Request.Method.GET,
                url,
                Producto[].class,
                new Response.Listener<Producto[]>() {
                    @Override
                    public void onResponse(Producto[] response) {

                        List<Producto> prods = Arrays.asList(response);
                        ProductosFragment.this.productos.setAdapter(new ProductoAdapter(prods));

                        callback.onDetailClick(prods.get(0));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(
                                getActivity(),
                                "Error: " + error.getMessage(),
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });

        Volley.newRequestQueue(getActivity()).add(request);*/

        Intent alService = new Intent(getActivity(), DownloadService.class);

        alService.putExtra("url", url);

        //getActivity().startService(alService);
        //getActivity().startService(alService);
        getActivity().startService(alService);
        //getActivity().stopService(alService);

    }

    public void cargarProducto(){
        ProductoDao dao = new ProductoDao(getContext());

        productos.setAdapter(new ProductoAdapter(dao.findAll()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (OnDetailClickListener) context;
    }


    @Override
    public void onStart() {
        super.onStart();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(downdloadReceiver, intentFilter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(downdloadReceiver);
    }

    private class DowndloadReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, intent.getAction(), Toast.LENGTH_SHORT).show();

            cargarProducto();
        }
    }


}
