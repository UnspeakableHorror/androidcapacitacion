package com.example.tecno.fragmenttest.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import com.example.tecno.fragmenttest.R;
import com.example.tecno.fragmenttest.model.SettingsManager;
import com.example.tecno.fragmenttest.service.AlarmUtils;
import com.example.tecno.fragmenttest.service.DownloadService;
import com.example.tecno.fragmenttest.service.RegistrationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class SettingsActivity extends AppCompatActivity {

    private CheckBox onlyWifi;

    private RadioGroup syncGroup;

    private Button register;

    private ProgressBar progressBar;

    private RegistrationReceiver registrationReceiver;
    private IntentFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        registrationReceiver = new RegistrationReceiver();
        filter = new IntentFilter(RegistrationService.REGISTRATION_SUCCESS_ACTION);

        onlyWifi = (CheckBox) findViewById(R.id.only_wifi);

        SettingsManager settingsManager = SettingsManager.getInstance(this);

        onlyWifi.setChecked(settingsManager.isOnlyWifi());

        onlyWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsManager.getInstance(SettingsActivity.this).setOnlyWifi(isChecked);
            }
        });

        syncGroup = (RadioGroup) findViewById(R.id.rg1);


        switch (SettingsManager.getInstance(SettingsActivity.this).getSynchronizationTime()){
            case 15:
                syncGroup.check(R.id.rb15);
                break;
            case 30:
                syncGroup.check(R.id.rb30);
                break;
            case 60:
                syncGroup.check(R.id.rb60);
                break;
            case 120:
                syncGroup.check(R.id.rb120);
                break;
            default:
                syncGroup.check(R.id.rb15);
                break;
        }


        syncGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int time = 0;

                switch (checkedId) {
                    case R.id.rb15:
                        time = 15;
                        break;
                    case R.id.rb30:
                        time = 30;
                        break;
                    case R.id.rb60:
                        time = 60;
                        break;
                    case R.id.rb120:
                        time = 120;
                        break;
                }

                Log.v("SettingsActivity", "Created alarm");

                AlarmUtils.createAlarm(SettingsActivity.this, time);
                SettingsManager.getInstance(SettingsActivity.this).setSynchronizationTime(time);
            }
        });

        register = (Button) findViewById(R.id.register_push_notification);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                startService(new Intent(SettingsActivity.this, RegistrationService.class));

            }
        });

        progressBar = (ProgressBar) findViewById(R.id.registration_progress);

        if(SettingsManager.getInstance(this).getFcmRegistraton()) {
            register.setText("Registrado Correctamente.");
            register.setEnabled(false);
        }

        checkGooglePlayServices();

    }


    private void checkGooglePlayServices(){
        GoogleApiAvailability availability = GoogleApiAvailability.getInstance();

        int result = availability.isGooglePlayServicesAvailable(this);

        if(result != ConnectionResult.SUCCESS){
            if(availability.isUserResolvableError(result)){
                register.setText("Google Play Services no disponible.");
                register.setEnabled(false);
            }

        }

    }


    public class RegistrationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            register.setText("Registrado Correctamente.");
            register.setEnabled(false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(registrationReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(registrationReceiver);
    }

    //    private void createAlarm(int time){
//        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        //en minutos
//        //long timeMillis = SystemClock.elapsedRealtime() + time * 60 * 1000;
//
//        PendingIntent pi = PendingIntent.getService(
//                this,
//                0,
//                new Intent(this, DownloadService.class),
//                PendingIntent.FLAG_UPDATE_CURRENT);
//
//        //en segundos
//        long timeMillis = SystemClock.elapsedRealtime() +  20 * 1000;
//
//        alarmManager.cancel(pi);
//        //alarmManager.set(AlarmManager.ELAPSED_REALTIME, timeMillis, pi);
//
//        // en forma indefinida
//        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, timeMillis, 20 * 1000, pi);
//
//    }

}
