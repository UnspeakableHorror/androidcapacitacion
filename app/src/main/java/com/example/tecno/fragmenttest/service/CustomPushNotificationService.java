package com.example.tecno.fragmenttest.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.tecno.fragmenttest.R;
import com.google.android.gms.gcm.GcmListenerService;

import java.util.Date;

/**
 * Created by cgagliardi on 08/11/2016.
 */

public class CustomPushNotificationService extends GcmListenerService {

    private static final String TAG = CustomPushNotificationService.class.getName();

    @Override
    public void onMessageReceived(String message, Bundle bundle) {
        super.onMessageReceived(message, bundle);
        Log.v(TAG, message);

        showNotification(message);
    }

    private void showNotification(String message){
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Sincronización")
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setAutoCancel(true)
                .build();

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(1, notification);
    }
}
