package com.example.tecno.fragmenttest.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.tecno.fragmenttest.model.SettingsManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by cgagliardi on 08/11/2016.
 */

public class RegistrationService extends IntentService {

    private static final String SENDER_ID = "301054889845";

    private static final String TAG = RegistrationService.class.getName();

    public static final String REGISTRATION_SUCCESS_ACTION = "com.example.tecno.fragmenttest.service.REGISTRATION_SUCCESS_ACTION";

    public RegistrationService(){
        super(RegistrationService.class.getName());
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RegistrationService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        InstanceID instanceID = InstanceID.getInstance(this);

        String token = null;

        try {
            token = instanceID.getToken(SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.v(TAG, "GCM Registration Token: " + token);

            SettingsManager.getInstance(this).setFcmRegistration(true);
        } catch (IOException e) {
            e.printStackTrace();
            SettingsManager.getInstance(this).setFcmRegistration(false);
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(REGISTRATION_SUCCESS_ACTION));
    }
}
