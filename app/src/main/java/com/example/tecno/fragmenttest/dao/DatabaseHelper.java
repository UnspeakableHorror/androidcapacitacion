package com.example.tecno.fragmenttest.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.tecno.fragmenttest.model.Producto;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by cgagliardi on 25/10/2016.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    static String DB_NAME = "Ejemplo.sqlite";

    static int DB_VERSION = 1;

    DatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Producto.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }
}
