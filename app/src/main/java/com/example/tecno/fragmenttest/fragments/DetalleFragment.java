package com.example.tecno.fragmenttest.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tecno.fragmenttest.activity.DetalleActivity;
import com.example.tecno.fragmenttest.R;
import com.example.tecno.fragmenttest.model.Producto;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

/**
 * Created by cgagliardi on 18/10/2016.
 */

public class DetalleFragment extends Fragment {

    private ImageView productImage;

    private TextView productId;

    private TextView productName;

    private TextView productPrice;

    private DisplayImageOptions displayImageOptions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detalle, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        displayImageOptions = new DisplayImageOptions.Builder()
                .displayer(new CircleBitmapDisplayer())
                .build();

        productImage = (ImageView) view.findViewById(R.id.detalle_product_image);
        productId = (TextView) view.findViewById(R.id.detalle_productoid);
        productName = (TextView) view.findViewById(R.id.detalle_productonombre);
        productPrice = (TextView) view.findViewById(R.id.detalle_productoprecio);

        //viene de un celular
        if(getArguments() != null){
            Producto producto = (Producto) getArguments().getSerializable(DetalleActivity.PROD);

            mostrarDetalle(producto);
        }
    }

    public void mostrarDetalle(Producto producto){
        productId.setText(String.valueOf(producto.getId()));

        ImageLoader.getInstance().displayImage(producto.getImage(), productImage, displayImageOptions);

        productName.setText(producto.getNombre());
        productPrice.setText(String.valueOf(producto.getPrecio()));
    }

    public static DetalleFragment newInstance(Bundle args) {

        DetalleFragment fragment = new DetalleFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
