package com.example.tecno.fragmenttest.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tecno.fragmenttest.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

import java.util.List;

/**
 * Created by cgagliardi on 18/10/2016.
 */

public class ProductoAdapter extends BaseAdapter {

    private List<Producto> productos;

    private DisplayImageOptions displayImageOptions;

    public ProductoAdapter(List<Producto> productos) {
        this.productos = productos;

        displayImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .displayer(new CircleBitmapDisplayer())
                .build();
    }

    @Override
    public int getCount() {
        return productos.size();
    }

    @Override
    public Object getItem(int position) {
        return productos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return productos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.adapter_producto, parent, false);

        Producto producto = (Producto) getItem(position);

        TextView id = (TextView) convertView.findViewById(R.id.product_id);
        id.setText(String.valueOf(producto.getId()));

        TextView nombre = (TextView) convertView.findViewById(R.id.product_nombre);
        nombre.setText(producto.getNombre());

        ImageView image = (ImageView) convertView.findViewById(R.id.image);

        if(image == null)
            throw new IllegalArgumentException("AAAAAAAAAAA");

        ImageLoader.getInstance().displayImage(producto.getImage(), image, displayImageOptions);

        return convertView;
    }
}
