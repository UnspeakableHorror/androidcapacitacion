package com.example.tecno.fragmenttest.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/**
 * Created by cgagliardi on 01/11/2016.
 */

public class AlarmUtils {
    public static void createAlarm(Context ctx, int synchronizationTime) {

        if(synchronizationTime == -1)
            return;

        AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        //en minutos
        long timeMillis = SystemClock.elapsedRealtime() + synchronizationTime * 60 * 1000;

        PendingIntent pi = PendingIntent.getService(
                ctx,
                0,
                new Intent(ctx, DownloadService.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        //en segundos
        //long timeMillis = SystemClock.elapsedRealtime() + 20 * 1000;

        alarmManager.cancel(pi);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME, timeMillis, pi);

        // en forma indefinida
        //alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, timeMillis, 20 * 1000, pi);
    }
}
