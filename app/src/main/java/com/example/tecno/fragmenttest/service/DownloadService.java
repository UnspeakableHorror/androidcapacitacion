package com.example.tecno.fragmenttest.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.tecno.fragmenttest.R;
import com.example.tecno.fragmenttest.activity.HomeActivity;
import com.example.tecno.fragmenttest.model.GsonRequest;
import com.example.tecno.fragmenttest.model.Producto;
import com.example.tecno.fragmenttest.dao.ProductoDao;
import com.example.tecno.fragmenttest.model.SettingsManager;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by cgagliardi on 25/10/2016.
 */

public class DownloadService extends Service implements Response.Listener<Producto[]>, Response.ErrorListener {

    private static final String TAG = "DownloadService";

    public static String ACTION_DOWNLOAD_SUCCESS = "com.example.tecno.fragmenttest.fragments.DOWNLOAD_SUCCESS";

    public static String ACTION_DOWNLOAD_ERROR = "com.example.tecno.fragmenttest.fragments.DOWNLOAD_ERROR";

    private GsonRequest<Producto[]> productRequest;
    private String url;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, this.getClass().getName() +  " onCreate");
        //"http://www.webkathon.com/pruebasit/products.php"
        productRequest = new GsonRequest<>(
                Request.Method.GET,
                "http://www.webkathon.com/pruebasit/products.php",
                Producto[].class,
                this,
                this);

        AlarmUtils.createAlarm(this, SettingsManager.getInstance(this).getSynchronizationTime());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "onBind");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.v(TAG, "onStartCommand " + startId);
        //Log.v(TAG, "onStartCommand " + intent.getExtras().get("url"));
        //url = intent.getExtras().get("url").toString();

        Log.v(TAG, "downloading");

        Volley.newRequestQueue(this).add(productRequest);

        //new Download().execute(1000*startId, startId);

        //return START_STICKY se ejecuta siempre

        return START_NOT_STICKY;
       // return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.v(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy");
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        Intent errorEvent = new Intent(ACTION_DOWNLOAD_SUCCESS);

        LocalBroadcastManager.getInstance(this).sendBroadcast(errorEvent);

        Log.v(TAG, "Download error.");
        stopSelf();
    }

    @Override
    public void onResponse(Producto[] response) {

        ProductoDao dao = new ProductoDao(this);

        for (Producto producto : response)
            dao.save(producto);

        Intent successEvent = new Intent(ACTION_DOWNLOAD_SUCCESS);

        LocalBroadcastManager.getInstance(this).sendBroadcast(successEvent);

        Log.v(TAG, "Download complete.");

        this.showNotificaton();

        this.stopSelf();
    }

    private class Download {

        public void execute(final long duration, final int startId){

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {

                    try {
                        Thread.sleep(duration);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    Log.v(TAG, "Download complete.");

                    stopSelf(startId);
                }
            }.execute();
        }

    }

    private void showNotificaton(){

        SimpleDateFormat format = new SimpleDateFormat("HH:mm");

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent in = new Intent(this, HomeActivity.class );
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pi = PendingIntent.getActivity(this, 0,
                in,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Sincronización")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText("Se realizó la sincronización a las " + format.format(new Date()))
                .setSound(sound)
                .setProgress(100, 50, false)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContentIntent(pi)
                .setAutoCancel(true)
                .build();

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(0, notification);

    }
}
