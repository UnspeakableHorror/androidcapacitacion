package com.example.tecno.fragmenttest.service;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by cgagliardi on 08/11/2016.
 */

public class CustomInstanceIDListenerService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        //cuando expira volver a pedir token
    }
}
