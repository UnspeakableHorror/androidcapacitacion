package com.example.tecno.fragmenttest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tecno.fragmenttest.R;

/**
 * Created by cgagliardi on 12/10/2016.
 */

public class LoginActivity extends AppCompatActivity {

    public static final String USER = "user";

    private EditText user;
    private EditText password;

    private Button login;
    private Button register;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user = (EditText) findViewById(R.id.login_user);
        password = (EditText) findViewById(R.id.login_password);

        login = (Button) findViewById(R.id.login_button);
        register = (Button) findViewById(R.id.login_register_button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(
                        LoginActivity.this,
                        user.getText().toString(),
                        Toast.LENGTH_SHORT)
                        .show();

                goHomeScreen();

                // con esto no se vuelve a esta pantalla desde la siguiente al presionar back.
                finish();
            }
        });
    }

    private void goHomeScreen(){
        Bundle bundle = new Bundle();
        bundle.putString(USER, user.getText().toString());

        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.putExtras(bundle);
        this.startActivity(homeIntent);
    }
}
