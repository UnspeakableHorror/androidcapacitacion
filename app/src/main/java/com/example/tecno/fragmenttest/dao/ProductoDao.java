package com.example.tecno.fragmenttest.dao;

import android.content.Context;

import com.example.tecno.fragmenttest.model.Producto;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * Created by cgagliardi on 25/10/2016.
 */

public class ProductoDao {

    private Dao<Producto, Integer> dao;

    public ProductoDao(Context context) {

        try {
            dao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Producto.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void save(Producto producto){

        try {
            dao.createOrUpdate(producto);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public List<Producto> findAll() {

        try {
            return dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
