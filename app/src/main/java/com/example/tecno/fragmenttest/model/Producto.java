package com.example.tecno.fragmenttest.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by cgagliardi on 18/10/2016.
 */
@DatabaseTable(tableName = "productos")
public class Producto implements Serializable {

    @DatabaseField(id = true)
    private int id;

    @DatabaseField
    @SerializedName("description")
    private String nombre;

    @DatabaseField
    private String image = "";

    @DatabaseField
    @SerializedName("price")
    private double precio;

    public Producto(){
    }

    public Producto(int id, String nombre, double precio) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
